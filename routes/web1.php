<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

class Lock
{
    public $n;
    protected $pressed = [];
    protected $comb = [];
    protected $available = []; // множество доступных для нажатия кнопок
    public $attempts = 1;

    function __construct($n = 1, $pressed = [], $available = null) {
        $this->n = $n;

        $this->pressed = $pressed;
        if($available)
            $this->available = $available;
        else
            for ($i = 0; $i < $n; $i++)
                $this->available[] = $i;
    }


    public function reset($k)
    {
        $this->available = [];
        $this->pressed = [];

        for ($i = 0; $i < $this->n; $i++) {
            $this->available[] = $i;
        }

        for($i = $this->n; $i > $k; $i--) {
            unset($this->available[$i - 1]);
            $this->pressed[] = $i - 1;
        }

        $this->attempts++;
        echo "reset. <br>";
    }

    function opened()
    {
        if(count($this->comb) == (2**$this->n - 1)) { // all comb
            echo "Opened! <br>";
            return true;
        }
        else return false;
    }

    function press($button, $k)
    {
        echo 'p b = ';
//        $this->printButtons();

        var_dump($button);
//        echo "<br> av = ";
//        var_dump($this->available);
//        echo "<br>";

        if(in_array($button, $this->available)) {
            $newPressed = $this->pressed;
            $newPressed[] = $button;
            sort($newPressed);

//            echo "in avai<br>";

//            echo "newPressed<br>";
//            var_dump($newPressed);
//            echo "<br>comb<br>";
//            var_dump($this->comb);
//            echo "<br>";

            if (!in_array($newPressed, $this->comb)) {
                $this->pressed = $newPressed;
                unset($this->available[$button]);
                $this->comb[] = $newPressed;
                echo "pressed<br>";
                $this->printButtons();

                if($this->opened())
                    return $this;
                else {
                    $available = $this->available;
                    $found = false;
                    foreach ($available as $a) {
                        $res = $this->press($a, $k);
                        if ($this->opened()) {
                            return $this;
                        }
                        if(!is_null($res))
                            $found = true;
                    }


                    if($found)
                        return $this;
                    else
                        return null;
                }
            }
            else {
                echo "povtor<br>";
//                echo "not in array b = ";
//                var_dump($button);
//                echo "<br> av = ";
//                var_dump($this->available);
//                echo "<br>";

                unset($this->available[$button]);
                if(count($this->available) > 0)
                    return $this->press(reset($this->available), $k);
                else
                    return null;
            }

        }
        else {
            echo "not avai<br>";
            return null;
        }


    }

    public function printButtons()
    {
        echo 'B: ';
        for ($i = 0; $i < $this->n; $i++)
            echo (in_array($i, $this->pressed)) ? '1 ' : '0 ';
        echo 'A: ';
        foreach ($this->available as $a)
            echo $a.' ';
        echo '<br>';
    }

    public function printComb()
    {
        echo 'Comb: ('.count($this->comb).')<br>';
        foreach ($this->comb as $c) {
            echo " _ ";
            for ($i =0; $i < $this->n; $i++)
                echo (in_array($i, $c)) ? '1 ' : '0 ';
            echo "<br>";
        }

        echo '<br>';
    }

    public function search()
    {
        for($k = $this->n; $k >= 0; $k--) {
            for ($button = 0; $button < $this->n; $button++) {
                $res = $this->press($button, $k);
                if ($this->opened()) {
                    exit;
                } else
                    if(is_null($res)) {
                        $this->reset($k);
                        continue;
                    }
            }


        }
    }
}

Route::get('/', function () {
    $n = 5;
    $lock = new Lock($n);

    $lock->printButtons();

    echo '<br>_start_<br>';
//    $newLock = $lock->press(1);
//    $newLock->printButtons();

    echo '<br>_search_<br>';

    $lock->search();

    echo "n = $n; attempts = $lock->attempts<br>";
    $lock->printComb();

    dd($lock);



//    return view('welcome');
});
