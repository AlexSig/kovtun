<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

class Lock
{
    public $n;
    public $attempts = 1;
    public $combinations = [];


//    protected $pressed = [];
//    protected $comb = [];
//    protected $available = []; // множество доступных для нажатия кнопок



    function __construct($n = 1) {
        $this->n = $n;

        $c = 0;

        for($row = 0; $row < 2**$n; $row++) {
            $r = [];
            $sum = 0;
            for ($col = 0; $col < $n; $col++) {
                $r[$col] = ($c >> ($col)) % 2;
                $sum += $r[$col];
            }

            $this->combinations[$sum][] = $r;
            $c++;
        }

        $max = 0;
        for($row = 0; $row < $n; $row++) {
            $len = count($this->combinations[$row]);
            if($len > $max)
                $max = $len;
        }

        $this->attempts = $max;
    }
}

Route::get('/', function () {
    for ($n = 2; $n <= 10; $n++){
        $lock = new Lock($n);
        dump($lock);
    };


//    $lock->printButtons();
//
//    echo '<br>_start_<br>';
//
//
//    echo '<br>_search_<br>';
//
//    $lock->search();
//
//    echo "n = $n; attempts = $lock->attempts<br>";
//    $lock->printComb();

//    dd($lock);



//    return view('welcome');
});
